# Lost In Translation

This is a demo app that anyone call `clone` and then play around with. The app has several views, use the navbar on the homepage to cycle through them. The website can be reached through this link: https://lostintranslation-hh.herokuapp.com/

## Install

```
git clone https://gitlab.com/hassanyaasin11/lostintranslation.git
cd lostintranslation
npm install
```

## Usage

```
npm start
```

This will open a new Webpage in your browser at `localhost:3000`. Remember to use your React and Context API browser extentions

## Contributors 

[Hassan Ali Yaasin (@hassanyaasin11)](@hassanyaasin11)

[Hussein Mohamed (@huah1600.hmah)](@huah1600.hmah)

## Licence 

Copyright 2022, Noroff Accelerate AS
