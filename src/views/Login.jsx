import LoginForm from "../components/Login/LoginForm"
import './Login.css'
import Logo from './Logo.png'
/**
 * This function displays the login page for the user to see.
 * User should be able to write a username of at least three signs
 * and login from this page by clicking on the "continue" button.
 * Clicking on the "Continue" button
 * should instantiate another function; "Loginform" function in the 
 * "Login" folder. This is called a "closure"; 
 * function returning another function.  
 * @returns Loginform function, function happens after click, 
 * Login header visible on the page.
 * visible on the page.
 */
const Login = () => {
    return (
        <>        
        <div className="App"> 
        <div className="orangeHeader">
        <img src={Logo} className ="logos" alt="ho"/>
        </div>
        <LoginForm/>
        <div className=" headText"><p>Lost in Translation</p> </div>
        
        <div className=" Lineone"> </div>
       
        <div className="intro-header"><h1>Lost In Translation</h1></div>
        <h2 className="intro-text">Get Started</h2>
        
        </div>
       
      
        </>
    )
}

export default Login