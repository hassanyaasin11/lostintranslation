import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileActions from "../components/Profile/ProfileActions"
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { userById } from "../api/user"
import { storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useEffect } from "react"

import '../components/Profile/profile.css'

/**
 * The profile of the user should be displayed through this function,
 * this happens when the "Continue" butten is clicked on after a username
 * is written.
 * @returns Username of the user and history of the what
 * the user has been translating on the application.
 */


const Profile = () => {

    const { user, setUser } = useUser()

    useEffect(() => {

        const finduser = async () => {
            const [error, latestUser] = await userById(user.id)
            if (error === null) {
                storageSave(STORAGE_KEY_USER, latestUser)
                setUser(latestUser)
            }
        }
        // findUser()
    }, [setUser, user.id])

    return (
        <>
                <div className="orangeHeaderTwo">
                <h1>Profile</h1>
                <ProfileHeader username={user.username} />
                <ProfileActions />
                <ProfileTranslationHistory translations={user.translations} />
                </div>
        </>
    )
}

export default withAuth(Profile)

