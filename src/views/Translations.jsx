import { useState } from "react"
import { translationAdd, translator } from "../api/translation"
import TranslationsForm from "../components/Translations/TranslationsForm"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage"
import Logo from './Logo.png'

const Translations = () => {

    let name = []
    let image = []

    //user
    const { user, setUser } = useUser()
    const [images, setImages] = useState()

    //This function translates the text typed in the input field to
    // sign language for the user to see.
    const translator = (text) => {

        name = text.toLowerCase().split("");
        image = name.map(x => "img/" + x + ".png")
        setImages(image)
    }

    const handleTranslate = async (notes) => {
        console.log(notes)
        const translation = notes.trim()
        console.log(notes)

        translator(notes)

        const [error, updatedUser] = await translationAdd(user, translation)
        if (error !== null) {

        }

        //Keep UI state and Server state in sync.
        storageSave(STORAGE_KEY_USER, updatedUser)
        // Update context state.
        setUser(updatedUser)

    }

    return (
        <>




 <div className="orangeHeadertwo"></div>
            <section>
            </section>
            <img src={Logo} className ="logos" alt="ho"/>


            <section id="translation-notes">
                <TranslationsForm onTranslation={handleTranslate} />
                {images && images.map((x) => <img src={x} alt={name} />)}
            </section>
            <div className=" Lineone"> </div>
            

        </>
    )
}

export default withAuth(Translations)