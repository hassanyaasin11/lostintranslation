/**
 * These are functions for saving to, reading from and deleting from storage.
 * There "localStorage" and and "sessionStorage" in the browser, the one that
 * will be used for the login this project is the session storage. Compared to localstorage,
 * session storage automatically deletes user from storage when the is logged out from the profile.
 */


/**
 * This functions saves the user to the storage when logged in.
 * @param {username} key 
 * @param {data of the user} value 
 */
export const storageSave = (key, value) => {
    sessionStorage.setItem(key, JSON.stringify(value))
}

/**
 * This function reads the necessary data based on the username when the user logs in.
 * This is how the information of the suer is displayed on the "profile" page,
 * for example the text the user has been translating.
 * @param {username} key 
 * @returns 
 */
export const storageRead = key => {
    const data = sessionStorage.getItem(key)

    if(data) {
        return JSON.parse(data)
    }
    return null
}

/**
 * This function deletes the user from the system,
 * for instance when the user logs out by pressing on
 * the "Logout" page.
 * @param {username} key 
 */
export const storageDelete = key =>{
    sessionStorage.removeItem(key)
}