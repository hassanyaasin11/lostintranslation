import { Link } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../utils/storage"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { translationClearHistory } from "../../api/translation"

import './profile.css'

const ProfileActions = () => {

    const { user, setUser } = useUser()

    const handleLogoutClick = () => {
        if (window.confirm('Are you sure?')) {

            //This function will delete "key_user" and set the user to null.
            //This will force to redirect to the login page
            //through the withAuth.jsx file in the "hoc" folder.
            //Notify the "withAuth" is a higher order component (hoc).
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    const handleClearHistoryClick = async () => {
        if (!window.confirm('Are you sure?\nThis can not be undone!')) {
            return
        }

        const [clearError] = await translationClearHistory(user.id)

        if (clearError !== null) {
            // Do something about this!
            return
        }

        const updatedUser = {
            ...user,
            translations: []
        }

        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
    }
    return (
        <>
            {/* <span><Link to="/translations" className="translationsLink">Translations</Link></span> */}
            <span><a href="/translations" className="translationsLink">Translations</a></span>
            <br></br><br></br>
            <span><button className="buttonClearHistory" onClick={handleClearHistoryClick}>Clear history</button></span>
            <br></br><br></br>
            <span><button className="buttonLogout2" onClick={handleLogoutClick}>Logout</button></span>
        </>
    )
}
export default ProfileActions