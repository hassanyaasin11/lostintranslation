import './Translation.css'
import { useForm } from "react-hook-form"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../utils/storage"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { Link, useNavigate } from 'react-router-dom'
/*import { translationClearHistory } from "../../api/translation"*/
const TranslationsForm = ({ onTranslation }) => {

    const { register, handleSubmit } = useForm()

    const onSubmit = ({ translationNotes }) => { onTranslation(translationNotes) }

    const { user, setUser } = useUser()

    const navigate = useNavigate()

    //Function making the user to log out
    const handleLogoutClick = () => {
        if (window.confirm('Are you sure?')) {

            //This function will delete "key_user" and set the user to null.
            //This will force to redirect to the login page
            //through the withAuth.jsx file in the "hoc" folder.
            //Notify the "withAuth" is a higher order component (hoc).
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }
/*  
    const handleClearHistoryClick = async () => {
        if(!window.confirm('Are you sure?\nThis can not be undone!')){
        return
        }

        const[ clearError ] = await translationClearHistory(user.id)

        if(clearError!== null){
           // Do something about this!
            return
        }

        const updatedUser = {
            ... user,
            translations:[]
        }

        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
    }
    */
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="Boxer ">
               {/*   <label htmlFor="translation-notes">⌨|</label>*/}
                <input className="inputfield"
                    type="text"{...register('translationNotes')}
                    placeholder="Translate..." />

            </div>

            <button className="buttonone" type="submit"></button>
            <div className="arrowone"></div>
            <a className='buttonLogout' href=" " onClick={handleLogoutClick}>  <div class="logout">LOGOUT</div></a>
            
            {/* <a className='buttonProfile'><li className="profile" ><Link to="/profile">Profile</Link></li></a> */}
            <a className='buttonProfile' href="/profile">  <div class="profile">PROFILE</div></a>
        </form>
        
        
    )

    
}
export default TranslationsForm