import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { loginUser } from '../../api/user'
import { storageSave } from "../../utils/storage";
import { useNavigate } from 'react-router-dom'
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import './loginform.css'
/**
 * This is where the functions for the LogInform are stored
 */


//The minimum requirements the user needs to meet in order to create a username.
const usernameConfig = {
    required: true,
    minLength: 3,
};

const LoginForm = () => {
    // Hooks
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { user, setUser } = useUser()
    const navigate = useNavigate()

    // Local State
    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)

    // Side Effects
    useEffect(() => {
        if (user !== null)
            navigate('profile')        // redirect to Profile.
    }, [user, navigate])  // Empty Dependencies - Only run once

    // Event Handlers
    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [error, userResponse] = await loginUser(username)
        if (error !== null) {
            setApiError(error)
        }

        if (userResponse !== null)
            storageSave(STORAGE_KEY_USER, userResponse)
        setUser(userResponse)

        setLoading(false)
    };

    // Render Functions

    //The message the login page will show in case of error; minmum requirements not met.
    //Minmum requirments not met; username should include signs of at least 3.
    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }

        //How the error message will look like in login page for an empty username.
        if (errors.username.type === 'required') {
            return <span>Username is required</span>
        }

        //How the error message will look like in login page for username less than 3 signs.
        if (errors.username.type === 'minLength') {
            return <span>Username is too short (minimum 3)</span>
        }
    })()

    //This returns the display of the login page 
    //that is viewable for the user.
    return (
        <>
         <div> 
            {/* HTML content in the page */}
           
            <form onSubmit={handleSubmit(onSubmit)}>
            
                <fieldset  className="Rectang ">
                <div className="Rectangthree"></div>
                    {/*  <label htmlFor="username">Username: </label>*/}
                   
                    <input
                    className="Rectangtwo"
                        type="text"
                        
                        placeholder="What's your name?..."
                        
                        {...register("username", usernameConfig)}
                    />
                    {errorMessage}

                </fieldset>
                
                <button className="button" type="submit" disabled={loading} ></button>
                <div className="arrow"></div>
                {loading && <p>Logging in...</p>}
                {apiError && <p>{apiError}</p>}
            </form>
            </div>
        </>
    )
}

export default LoginForm;