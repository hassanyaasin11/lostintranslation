import { Navigate } from "react-router-dom"
import { useUser } from "../context/UserContext"

/**
 * The withAuto function has a purpose to protect routes.
 * When the user is logged out from system (localstorage),
 * the user should be navigated to startpage; the login page. 
 * @param {*} Component 
 * @returns navigation to startpage; Loginpage
 */
const withAuth = Component => props => {
    const { user } = useUser()
    //The page will re-render and redirect to 
    //the startpage when the user changes
    if (user !== null) {
        return <Component{...props} />
    } else {
    }
    return <Navigate to="/" />
}
export default withAuth